import pygame, sys
from pygame.locals import *

pygame.init()

class Map(object):
    def __init__(self, x, y, SURF):
        self.x = x
        self.y = y
        self.SURF = SURF

    def createNewMap(self, D1, D2):
        self.Map = []
        self.D1 = D1
        self.D2 = D2
        for n in range(self.D1):
            current = ['']*self.D2
            self.Map.append(current)
        return self.Map

    def drawMap(self):
        imgObj = pygame.image.load('grass.gif')
        imgObj = pygame.transform.scale(imgObj, (40, 40))
        xc = self.x
        yc = self.y

        for itemY in self.Map:
            for itemX in self.Map[self.Map.index(itemY)]:
                self.SURF.blit(imgObj, (xc, yc))
                xc += 40
            yc += 40
            xc = self.x








